####............................................................................
#### Wolf Litter Estimation Model ----
#### 
#### This model was developed by Zach Fogel and Matt Mumma for estimating the 
#### number and size of wolf litters in Idaho using Approximate Bayesian 
#### Computing (ABC)
#### 
#### Definition of terms used:
####    Actual/real data -- Real life mortality data
####    Whole population -- Entire population that YOY are harvested from
####    Sample/Sample population -- Subset of wolves that are harvested
####    Sampled -- Harvested
####    Simulated Population -- Populations created for determining which most 
####        closely resembles the actual data
####    True Population -- Populations created for testing model 
####    Parameters -- mu/sd YOY per litter and number of litters; randomly selected
####        for creating simulated and true populations 
####    Summary Statistics -- number of litters sampled from, mu/sd YOY sampled 
####        from each litter, # litters with 1 pup harvested, # litters with 2
####        pups harvested, # with 3 harvested, etc. 
####    Target/Target Statistics -- Summary Statistics for actual population
####    Sumstats -- Summary Statistics for Simulated population
####    Dirichlet/capwire Distribution -- distribution of number of litters with 
####        a given number of pups born/harvested. Ex: 1 1 2 5 3 means that 2 litters 
####        had one pup born/harvested, 1 litter had 2 pups born/harvested, 1 had
####        3, and 1 had 5. Max pups, number of pups, etc often refers to the max
####        number of pups from any one litter.


####............................................................................
#### Load Packages and Real Life Data ----
source('Scripts/Load_Packages.R')
## Data from actual harvest
source('Scripts/Load_Actual_Data.R')
## Helper functions
source('Scripts/helper_fcts.R')
## Priors
source('Scripts/Load_Priors.R')


####............................................................................
#### Define Priors ----
#### These will be used to build simulated populations 

####............................................................................
#### Run Model ----

#' Set number of simulations
num.trials <- 100

# put all of this in one bracket so it can all run at the same time
{
  # true.pups is the max number of pups sampled from a single litter for the 
  # real population
  # max.pups is the max number of pups sampled from a single litter for any 
  # population, real or simulated. This starts out the same as true.pups but is 
  # updated during the model
  max.pups <- true.pups
  min.pups <- true.pups
  
  #' Initialize empty summary table
  sumtbl <- data.frame(
                       # Parameters used to build each simulated population
                       param.mu = as.numeric(), 
                       param.sd = as.numeric(), 
                       param.litters = as.numeric(), 
                       # Harvest likelihoods used to determine probability of 
                       # sampling a given individual (used in sample.pop())
                       # lhl.mu = as.numeric(), 
                       # lhl.theta = as.numeric(),
                       ihl.sd = as.numeric(),
                       # Initial summary statistics (stats for number of litters with X 
                       # pups harvested are added later)
                       ss.mu = as.numeric(),
                       ss.sd = as.numeric(),
                       ss.lit = as.numeric()
                      ) 

  for (t in 1:num.trials) {
    if (t %% 1000 == 0) print(t)
    # Simulate population; function returns a population and descriptive statistics
    # for that population
    samplelist <- create.test.pop(priors)
    
    # Dataframe with a row for each YOY; dataframe gives which litter the pup came
    # from as well as its harvest likelihood
    df <- samplelist$df
    
    # Sample pups from the population that was just simulated 
    # n.hunted is the actual number of pups that were harvested in a given year,
    # true.tbl contains the dirichlet distribution for the actual data
    # sumstats contains the summary statistics of the sampled population for that
    # simulation
    sumstats <- sample.pop(df, n.hunted, true.tbl, priors)
    
    # sumstats length is max pups/litter + 3 (mu, sd, number of litters)
    if ((length(sumstats)-n.sumstats) > max.pups) max.pups <- length(sumstats) - n.sumstats
    # depending on how we want to truncate data, we might want to keep track of 
    # min.pups as well
    # if ((length(sumstats)-3) < min.pups) min.pups <- length(sumstats) - 3
    
    # newrow is a vector of the parameters used to build the simulated population
    # and the summary stats of the sampled population
    newrow <- c(samplelist$yoy.mu, samplelist$yoy.sd, samplelist$num.litters, 
                samplelist$ihl.sd, sumstats) %>% 
      # t() means transpose, turns column into row and vice versa (allows us to 
      # append newrow onto the end of sumtbl)
      base::t() %>%
      as.data.frame() %>%
      rename(param.mu = V1,
             param.sd = V2,
             param.litters = V3,
             lhl.mu = V4,
             lhl.theta = V5,
             ihl.sd = V6,
             ss.mu = V7,
             ss.sd = V8,
             ss.lit = V9)
    
    # bind newrow onto the end of sumtbl. rbind.fill fills in NAs if the dfs have
    # differing numbers of rows
    sumtbl <- plyr::rbind.fill(sumtbl, newrow)
  } # for
  
  # change NA to 0
  sumtbl[is.na(sumtbl)] <- 0
  # sumtblbackup <- sumtbl
  
  #' Assign names (for whatever reason the names keep disappearing)
  #' nl.pop is sample population number of litters
  #' u.pop is sample pop mean
  #' sd.pop is sample pop sd
  #' LHLSD is litter harvest likelihood sd
  #' IHLSD is individual harvest likelihood sd
  names(sumtbl) <- c(
    'param.mu', 'param.sd', 'param.lit', 'LHLMU', 'LHLTHETA', 'IHLSD',
    'ss.mu', 'ss.sd', 'ss.lit',  
    paste0(1:max.pups,'pups')
    # 'u.absdif', 'sd.absdif', 'sum.litdif'
  )
  
  sumtbl <- as_tibble(sumtbl)
  
  # if cutting off summary stats with the true population pups as the max:
  # if true pop max is 7 pups from one litter, then change '7pups' column to '7+'
  # there are 8 columns that aren't the number of pups from a litter (param.mu:ss.sd)
  # Note: You could do the same thing but instead of max pups from true data, you
  # could use the max # pups from the simulation with the smallest max #
  # names(sumtbl)[min.pups + 8] <- 'truepups'
  # # sumtbl$last <- 0
  # sumtbl <- sumtbl %>%
  #   dplyr::rowwise() %>%
  #   dplyr::mutate(truepups = sum(c_across(truepups:last_col()))) %>%
  #   dplyr::select( -c( paste0(min.pups+1,'pups'):last_col() )) %>%
  #   dplyr::ungroup()
}

####............................................................................
#### ABC Part ----

# target is the summary stats from the real population
# truepop.sumstats is the dirichlet distribution for the real population
target <- c(nl.true, u.true, sd.true, truepop.sumstats)
paramstats <- dplyr::select(sumtbl, param.mu, param.sd, param.lit)
# `1pups`:last_col() selects 1pups, the last column, and everything in between
sumstats <- dplyr::select(sumtbl, ss.mu, ss.sd, ss.lit, `1pups`:tidyselect::last_col())
# how many 1pups etc columns are there?
n.pups <- ncol(dplyr::select(sumstats, `1pups`:tidyselect::last_col()))


trueR05 <- test.true(priors, paramstats, sumstats, tolerance = .05, method = 'rejection', 100, max.pups)
trueL05 <- test.true(priors, paramstats, sumstats, tolerance = .05, method = 'loclinear', 100, max.pups)
trueN05 <- test.true(priors, paramstats, sumstats, tolerance = .05, method = 'neuralnet', 100, max.pups)
# trueRi05 <- test.true(priors, paramstats, sumstats, tolerance = .05, method = 'ridge', 100, max.pups)
tol.05 <- rbind(trueR05, trueL05, trueN05
                # trueRi05
                ) %>%
  as_tibble() %>%
  group_by(Method, Tolerance) %>%
  summarise(accuracy.mu = sum(accuracy.mu),
            accuracy.sd = sum(accuracy.sd),
            accuracy.lit = sum(accuracy.lit)) %>%
  ungroup()

trueR1 <- test.true(priors, paramstats, sumstats, tolerance = .1, method = 'rejection', 100, max.pups)
trueL1 <- test.true(priors, paramstats, sumstats, tolerance = .1, method = 'loclinear', 100, max.pups)
trueN1 <- test.true(priors, paramstats, sumstats, tolerance = .1, method = 'neuralnet', 100, max.pups)
# trueRi1 <- test.true(priors, paramstats, sumstats, tolerance = .1, method = 'ridge', 100, max.pups)
tol.1 <- rbind(trueR1, trueL1, trueN1 
               # trueRi1
               ) %>%
  as_tibble() %>%
  group_by(Method, Tolerance) %>%
  summarise(accuracy.mu = sum(accuracy.mu),
            accuracy.sd = sum(accuracy.sd),
            accuracy.lit = sum(accuracy.lit)) %>%
  ungroup()

trueR15 <- test.true(priors, paramstats, sumstats, tolerance = .15, method = 'rejection', 100, max.pups)
trueL15 <- test.true(priors, paramstats, sumstats, tolerance = .15, method = 'loclinear', 100, max.pups)
trueN15 <- test.true(priors, paramstats, sumstats, tolerance = .15, method = 'neuralnet', 100, max.pups)
# trueRi15 <- test.true(priors, paramstats, sumstats, tolerance = .15, method = 'ridge', 100, max.pups)
tol.15 <- rbind(trueR15, trueL15, trueN15 
                # trueRi15
                ) %>%
  as_tibble() %>%
  group_by(Method, Tolerance) %>%
  summarise(accuracy.mu = sum(accuracy.mu),
            accuracy.sd = sum(accuracy.sd),
            accuracy.lit = sum(accuracy.lit)) %>%
  ungroup()

trueR2 <- test.true(priors, paramstats, sumstats, tolerance = .2, method = 'rejection', 100, max.pups)
trueL2 <- test.true(priors, paramstats, sumstats, tolerance = .2, method = 'loclinear', 100, max.pups)
trueN2 <- test.true(priors, paramstats, sumstats, tolerance = .2, method = 'neuralnet', 100, max.pups)
# trueRi2 <- test.true(priors, paramstats, sumstats, tolerance = .2, method = 'ridge', 100, max.pups)
tol.2 <- rbind(trueR2, trueL2, trueN2
               # trueRi2
               ) %>%
  as_tibble() %>%
  group_by(Method, Tolerance) %>%
  summarise(accuracy.mu = sum(accuracy.mu),
            accuracy.sd = sum(accuracy.sd),
            accuracy.lit = sum(accuracy.lit)) %>%
  ungroup()

trueR3 <- test.true(priors, paramstats, sumstats, tolerance = .3, method = 'rejection', 100, max.pups)
trueL3 <- test.true(priors, paramstats, sumstats, tolerance = .3, method = 'loclinear', 100, max.pups)
trueN3 <- test.true(priors, paramstats, sumstats, tolerance = .3, method = 'neuralnet', 100, max.pups)
# trueRi3 <- test.true(priors, paramstats, sumstats, tolerance = .3, method = 'ridge', 100, max.pups)
tol.3 <- rbind(trueR3, trueL3, trueN3 
               # trueRi3
               ) %>%
  as_tibble() %>%
  group_by(Method, Tolerance) %>%
  summarise(accuracy.mu = sum(accuracy.mu),
            accuracy.sd = sum(accuracy.sd),
            accuracy.lit = sum(accuracy.lit)) %>%
  ungroup()

trueR35 <- test.true(priors, paramstats, sumstats, tolerance = .35, method = 'rejection', 100, max.pups)
trueL35 <- test.true(priors, paramstats, sumstats, tolerance = .35, method = 'loclinear', 100, max.pups)
trueN35 <- test.true(priors, paramstats, sumstats, tolerance = .35, method = 'neuralnet', 100, max.pups)
# trueRi35 <- test.true(priors, paramstats, sumstats, tolerance = .35, method = 'ridge', 100, max.pups)
tol.35 <- rbind(trueR35, trueL35, trueN35 
                # trueRi35
                ) %>%
  as_tibble() %>%
  group_by(Method, Tolerance) %>%
  summarise(accuracy.mu = sum(accuracy.mu),
            accuracy.sd = sum(accuracy.sd),
            accuracy.lit = sum(accuracy.lit)) %>%
  ungroup()

trueR4 <- test.true(priors, paramstats, sumstats, tolerance = .4, method = 'rejection', 100, max.pups)
trueL4 <- test.true(priors, paramstats, sumstats, tolerance = .4, method = 'loclinear', 100, max.pups)
trueN4 <- test.true(priors, paramstats, sumstats, tolerance = .4, method = 'neuralnet', 100, max.pups)
# trueRi4 <- test.true(priors, paramstats, sumstats, tolerance = .4, method = 'ridge', 100, max.pups)
tol.4 <- rbind(trueR4, trueL4, trueN4 
               # trueRi4
               ) %>%
  as_tibble() %>%
  group_by(Method, Tolerance) %>%
  summarise(accuracy.mu = sum(accuracy.mu),
            accuracy.sd = sum(accuracy.sd),
            accuracy.lit = sum(accuracy.lit)) %>%
  ungroup()

alltests <- rbind(tol.05, tol.1, tol.15, tol.2
                  # tol.3, tol.35, tol.4
                  ) %>%
  dplyr::mutate(pass = ifelse(accuracy.mu >= 95 & accuracy.sd >= 95 & accuracy.lit >= 95, 'pass', 'fail')) %>%
  as.data.frame() #%>%
  # filter(pass == 'pass')

if (length(target) < ncol(sumstats)) {
  target <- c(target, rep(0, ncol(sumstats) - length(target)))
}
abcR05 <- abc(target, param = paramstats, sumstat = sumstats, tol = .05, method = 'rejection')
summary(abcR05)

cvrej <- cv4abc(param = paramstats, sumstat = sumstats, abc.out = abcR05, nval = 100, tols = c(0.5,.1,.15,.2,.25,.3,.35))

abcR05$region
acceptedparams <- paramstats[which(abcR05$region),]
hist(acceptedparams$param.mu)
hist(acceptedparams$param.sd)
hist(acceptedparams$param.lit)

x <- paste(trueR05$accepted, collapse = ' ') %>%
  strsplit(split = ' ') %>%
  unlist() %>%
  as.numeric() %>%
  unique() %>% 
  sort()
